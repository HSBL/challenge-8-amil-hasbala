import React, { Component } from "react";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = { email: "", password: "", formSubmitted: false };
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }
  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }
  handleSubmit(event) {
    this.setState({ formSubmitted: true });
    event.preventDefault();
  }

  render() {
    return (
      <>
        {!this.state.formSubmitted && (
          <form onSubmit={this.handleSubmit}>
            <label>
              Email:
              <input
                type="email"
                value={this.state.email}
                onChange={this.handleEmailChange}
              />
            </label>
            <label>
              Password:
              <input
                type="password"
                value={this.state.password}
                onChange={this.handlePasswordChange}
              />
            </label>
            <div>
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </div>
          </form>
        )}

        {this.state.formSubmitted && (
          <div>
            <h1>Email: {this.state.email}</h1>
            <h1>Password: {this.state.password}</h1>
          </div>
        )}
      </>
    );
  }
}

export default Register;
