// import logo from "./logo.svg";
import React, { Component } from "react";
import "./App.css";
import Register from "./components/Register";

class App extends Component {
  state = {
    showing: "",
  };

  handleRegister = () => {
    this.setState({ showing: "register" });
  };
  handleEdit = () => {
    this.setState({ showing: "edit" });
  };
  handleSearch = () => {
    this.setState({ showing: "search" });
  };
  render() {
    return (
      // <div className="App">
      //   <header className="App-header">
      //     <img src={logo} className="App-logo" alt="logo" />
      //     <button className="btn btn-primary">Start React!</button>
      //   </header>
      // </div>
      <div className="App">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="container-fluid">
            <span class="navbar-brand" href="#">
              THE GAME
            </span>
            <button
              class="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <span class="nav-link active" aria-current="page" href="#">
                    Home
                  </span>
                </li>
                <li class="nav-item">
                  <span class="nav-link" onClick={this.handleRegister}>
                    Register
                  </span>
                </li>
                <li class="nav-item">
                  <span class="nav-link" onClick={this.handleEdit}>
                    Edit
                  </span>
                </li>
                <li class="nav-item">
                  <span class="nav-link" onClick={this.handleSearch}>
                    Search
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div>
          {!this.state.showing && <h1>HOME</h1>}
          {this.state.showing === "register" && <Register />}
          {this.state.showing === "edit" && <h1>From Edit</h1>}
          {this.state.showing === "search" && <h1>From Search</h1>}
        </div>
      </div>
    );
  }
}

export default App;
